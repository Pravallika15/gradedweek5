create database travel;

use travel;

create table PASSENGER
 (Passenger_name varchar(20), 
  Category               varchar(20),
   Gender                 varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
  Distance                int,
  Bus_Type             varchar(20)
);

create table PRICE
(
             Bus_Type   varchar(20),
             Distance    int,
              Price      int
          );

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;


========================================================================1===========================================================


mysql> select count(case when (Gender) = 'M' THEN 1 END) Male,
count(case when (Gender) = 'F' THEN 1 END) Female from passenger where Distance>=600;


========================================================================2============================================================


mysql> select
    -> min(Price) as Min_Price
    -> from price where Bus_Type='sleeper';


========================================================================3=============================================================

mysql> select Passenger_name
    -> from passenger
    -> where Passenger_name like 'S%'
    -> order by Passenger_name asc;

=======================================================================4=================================================================

mysql> select distinct passenger.Passenger_name,passenger.Boarding_City,passenger.Destination_City,
    -> passenger.Bus_Type,price.Price
    -> from passenger, price
    -> where passenger.Distance=price.Distance and passenger.Bus_Type=price.Bus_Type
    -> group by passenger.Passenger_name;


======================================================================5========================================================================

mysql> select passenger.Passenger_name,price.Price from passenger inner join price on passenger.Distance = 
    -> price.Distance where passenger.Bus_Type = 'Sitting' and passenger.Distance >= 1000;


======================================================================6=========================================================================

mysql>   select passenger.Passenger_name, 
    ->price.Bus_Type, passenger.Distance, price.Price
    ->from passenger
    ->left join price on passenger.Distance=price.Distance
    ->where passenger.Passenger_name='pallavi' and passenger.Boarding_City='Bengaluru' and passenger.Destination_City='panaji';

======================================================================7=========================================================================


mysql> select distinct Distance
    -> from passenger
    -> order by Distance desc;


======================================================================8==========================================================================

mysql> select  Passenger_name,(Distance/a.sum)*100 as percentage from passenger
    -> cross join (select sum(Distance) as sum from passenger)a;

=======================================================================9=============================================================================

mysql> create view passenger_in_AcBus as
    -> select * from passenger
    -> where Category='AC';
Query OK, 0 rows affected (0.16 sec)

mysql> select * from passenger_in_AcBus;

=======================================================================10================================================================================
mysql>delimiter //
    ->create procedure getp()
    -> begin
    -> select  count(*) AS Passenger_count
    -> from passenger
    -> where passenger.Bus_Type='sleeper';
    -> end
   //

 call getp();
   //

========================================================================11===============================================================================
mysql> select * from passenger limit 5;



mysql> select * from passenger limit 5 offset 5;


=========================================================================12====================================================================================



